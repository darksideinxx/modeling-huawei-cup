function [err1,err2,err3]=errcal(locest,locreal)

err1=abs(locest-locreal);
err2=norm(locest(1:2)-locreal(1:2));
err3=norm(locest(1:3)-locreal(1:3));
