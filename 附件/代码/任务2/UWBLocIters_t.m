function [Xres] = UWBLocIters_t(Obs,para)

BsNum=para.BsNum;
para2=para; 
para2.BsLoc=para.BsLoc;
for ii=1:BsNum
    Obs2=Obs;
    Obs2(ii)=Obs2(ii)-500;%�ĸ����� ��ȥ500
    Xres(ii,:) = UWBLocIters(Obs2,para2);%
    
    ObsE = UWBToaObs(para.BsLoc,para.BsNum,Xres(ii,:));
    Err1(ii) = sum(abs(Obs2-ObsE));
end
for ii=1:BsNum
    Obs2=Obs;
    Obs2(ii)=Obs2(ii)-350;
    Xres(ii+BsNum,:) = UWBLocIters(Obs2,para2);
    
    ObsE = UWBToaObs(para.BsLoc,para.BsNum,Xres(ii+BsNum,:));
    Err1(ii+BsNum) = sum(abs(Obs2-ObsE));
end

for ii=1:BsNum
    Obs2=Obs;
    Obs2(ii)=Obs2(ii)-650;
    Xres(ii+BsNum*2,:) = UWBLocIters(Obs2,para2);
    
    ObsE = UWBToaObs(para.BsLoc,para.BsNum,Xres(ii+BsNum*2,:));
    Err1(ii+BsNum*2) = sum(abs(Obs2-ObsE));
end

Obs2=Obs;
Xres(end+1,:) = UWBLocIters(Obs2,para2);

ObsE = UWBToaObs(para.BsLoc,para.BsNum,Xres(end,:));
Err1(end+1) = sum(abs(Obs2-ObsE));

[~,ErrInd]=min(Err1);
Xres=Xres(ErrInd,:);