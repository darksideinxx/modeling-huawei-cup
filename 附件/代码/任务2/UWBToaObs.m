function Res = UWBToaObs(BsLoc,BsNum,TargetLoc)
Res = zeros(1,BsNum);

% c = 3e8;
for ii=1:BsNum
     Res(ii) = norm(BsLoc(ii,:)-TargetLoc); 
end

end
 