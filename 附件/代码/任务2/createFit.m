function [fitresult, gof] = createFit(AA, BB)
%CREATEFIT(AA,BB)
%  Create a fit.
%
%  Data for 'untitled fit 1' fit:
%      X Input : AA
%      Y Output: BB
%  Output:
%      fitresult : a fit object representing the fit.
%      gof : structure with goodness-of fit info.
%
%  另请参阅 FIT, CFIT, SFIT.

%  由 MATLAB 于 14-Oct-2021 11:07:35 自动生成


%% Fit: 'untitled fit 1'.
[xData, yData] = prepareCurveData( AA, BB );

% Set up fittype and options.
ft = fittype( 'poly1' );

% Fit model to data.
[fitresult, gof] = fit( xData, yData, ft );

% Plot fit with data.
figure( 'Name', 'liner' );
h = plot( fitresult, xData, yData,'k.' );
legend( h, '实测值 vs. 观测值', 'liner', 'Location', 'NorthEast' );
% Label axes
xlabel 观测距离
ylabel 实测距离
grid on


