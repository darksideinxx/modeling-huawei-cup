clear;clc;close all;warning off;

Bsnum=4;
Bsloc=[0 0 1300;5000 0 1700;0 5000 1700;5000 5000 1300];

para.Thr = 100;
para.DataLen=1;

para.BsNum=Bsnum;
para.BsLoc=Bsloc;
para.MaxIters=100;
para.xinit = mean(Bsloc,1);
para.dRef=2500;

load fitfunz
Obs_10=[1320 3950 4540 5760;3580 2580 4610 3730;2930 2600 4740 4420;2740 2720 4670 4790;2980 4310 2820 4320;
    2230 3230 4910 5180;4520 1990 5600 3360;2480 3530 4180 5070;4220 2510 4670 3490;5150 2120 5800 2770;];


for ii=1:5
    Obsz=fitresult( Obs_10(ii,:));
    LocEst1(ii,:)= UWBLocIters(Obsz',para);
    fprintf('无干扰数据%d,估计位置[%01.0f,%01.0f,%01.0f]\n',ii, LocEst1(ii,1), LocEst1(ii,2), LocEst1(ii,3));
end

para2=para;para2.dRef=5e3;
for ii=6:10
    Obsz=fitresult( Obs_10(ii,:));
    LocEst1(ii,:)= UWBLocIters_t(Obsz',para2);
    fprintf('干扰数据%d,估计位置[%01.0f,%01.0f,%01.0f]\n',ii, LocEst1(ii,1), LocEst1(ii,2), LocEst1(ii,3));
end
