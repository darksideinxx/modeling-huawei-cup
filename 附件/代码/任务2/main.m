clear;clc;close all;warning off;

Bsnum=4;
Bsloc=[0 0 1300;5000 0 1700;0 5000 1700;5000 5000 1300];

para.Thr = 100;
para.DataLen=1;

para.BsNum=Bsnum;
para.BsLoc=Bsloc;
para.MaxIters=25;
para.xinit = mean(Bsloc,1);
para.dRef=2500;


locreal=locrealread('Tag坐标信息.txt');
locnum=size(locreal,1);


% filedir='10-16/正常数据_预处理';
% for i=1:locnum
%     filename=[num2str(i) '.正常.csv'];
%     or_data=csvread(fullfile(filedir,filename),1);%数据预处理
%     outres=or_data(:,2:5);
%     Obs_1(i,:)=UWBLocIters(outres,para)
%     t=or_data(:,1)';
% end

filedir='10-16/正常数据_预处理';
for i=1:locnum
    filename=[num2str(i) '.正常.csv'];
    or_data=csvread(fullfile(filedir,filename),1)%数据预处理
    outres=or_data(:,2:5);
    t=or_data(:,1)';
    Obs_1(i,:)=mean(outres,1);%四个距离的平均值（测量距离）
    Obs_2(i,:) = UWBToaObs(Bsloc,Bsnum,locreal(i,:)*10);%Tag坐标到四个锚点的距离（理论距离）
    Obs_1s(i).data=outres;%预处理数据
end


[fitresult] = createFit(Obs_1(:), Obs_2(:));%矩阵转为行向量
save fitfunz fitresult

%% 正常数据处理部分

% cnt=1;
% for ii=1:locnum
%     [err1(cnt,:),err2(cnt),err3(cnt)]=errcal(Obs_1(ii,:),locreal(ii,:)*10);
%     cnt=cnt+1;
% end
cnt=1;
for ii=1:locnum
    for jj=1:size(Obs_1s(ii).data,1)
        Obsz=fitresult( Obs_1s(ii).data(jj,:));
        LocEst(cnt,:)= UWBLocIters(Obsz',para);
        [err1(cnt,:),err2(cnt),err3(cnt)]=errcal(LocEst(cnt,:),locreal(ii,:)*10);
        cnt=cnt+1;
    end
end
figure;hold on;
xlabel('x坐标/mm');
ylabel('y坐标/mm');
zlabel('z坐标/mm');
axis([0,5000,0,5000,0,3000]);
hold on
plot3(locreal(:,1)*10,locreal(:,2)*10,locreal(:,3)*10,'gp');
plot3(LocEst(:,1),LocEst(:,2),LocEst(:,3),'r.');
plot3(Bsloc(:,1),Bsloc(:,2),Bsloc(:,3),'b^');
grid on 
legend('目标位置','估计位置','锚点位置');
axis([0,5000,0,5000,0,3000]);
title('直接定位');


figure;
for ii=1:3
    subplot(3,1,ii);
    Data=err1(:,ii);
    Dmax=min(max(Data),1000);
    DataL=0:(Dmax)/50:Dmax;
    Datahist=hist(Data,DataL);
    Datahist=Datahist/sum(Datahist);
%     Datahist=cumsum(Datahist);
    plot(DataL,Datahist);
    xlabel('误差大小/mm');ylabel('概率分布函数');
    axis([0 Dmax 0 1])
end
subplot(3,1,1);title(['正常数据x方向误差'])
subplot(3,1,2);title(['正常数据y方向误差'])
subplot(3,1,3);title(['正常数据z方向误差'])

figure;
Data=err2;
Dmax=min(max(Data),1000);
DataL=0:(Dmax)/50:Dmax;
Datahist=hist(Data,DataL);
Datahist=Datahist/sum(Datahist);
% Datahist=cumsum(Datahist);
plot(DataL,Datahist);
xlabel('误差大小/mm');ylabel('概率分布函数');
title(['正常数据二维误差'])

figure;
Data=err3;
Dmax=min(max(Data),2000);
DataL=0:(Dmax)/50:Dmax;
Datahist=hist(Data,DataL);
Datahist=Datahist/sum(Datahist);
% Datahist=cumsum(Datahist);
plot(DataL,Datahist);
xlabel('误差大小/mm');ylabel('概率分布函数');
title(['正常数据三维误差']);


%% 异常数据直接定位
filedir='10-16/异常数据_预处理';
for i=1:locnum
    filename=[num2str(i) '.异常.csv'];
    or_data=csvread(fullfile(filedir,filename),1)%数据预处理
    outres=or_data(:,2:5);
    t=or_data(:,1)';
    Obs_1s0(i).data=outres;%预处理数据
end



% filedir='异常数据';
% for ii=1:locnum
%     filename2=[num2str(ii) '.异常.txt'];
%     [outres,t]=fileread1(fullfile(filedir,filename2),Bsnum,1,[]);
%      Obs_1s0(ii).data=outres;
% end

cnt=1;
for ii=1:locnum
    for jj=1:size(Obs_1s0(ii).data,1)
        Obsz=fitresult( Obs_1s0(ii).data(jj,:));
        LocEst1(cnt,:)= UWBLocIters(Obsz',para);
        [err1(cnt,:),err2(cnt),err3(cnt)]=errcal(LocEst1(cnt,:),locreal(ii,:)*10);
        cnt=cnt+1;
    end
end

figure;hold on;
xlabel('x坐标/mm');
ylabel('y坐标/mm');
zlabel('z坐标/mm');
axis([0,5000,0,5000,0,3000]);
hold on
plot3(locreal(:,1)*10,locreal(:,2)*10,locreal(:,3)*10,'gp');
plot3(LocEst1(:,1),LocEst1(:,2),LocEst1(:,3),'r.');
plot3(Bsloc(:,1),Bsloc(:,2),Bsloc(:,3),'b^');


grid on 
legend('目标位置','估计位置','锚点位置');
axis([0,5000,0,5000,0,3000]);
title(['异常数据直接定位']);

figure;
for ii=1:3
    subplot(3,1,ii);
    Data=err1(:,ii);
    Dmax=min(max(Data),1000);
    DataL=0:(Dmax)/50:Dmax;
    Datahist=hist(Data,DataL);
    Datahist=Datahist/sum(Datahist);
%     Datahist=cumsum(Datahist);
    plot(DataL,Datahist);
    xlabel('误差大小/mm');ylabel('概率分布函数');
    axis([0 Dmax 0 1])
end
subplot(3,1,1);title(['异常数据直接定位x方向误差'])
subplot(3,1,2);title(['异常数据直接定位y方向误差'])
subplot(3,1,3);title(['异常数据直接定位z方向误差'])

figure;
Data=err2;
Dmax=min(max(Data),1000);
DataL=0:(Dmax)/50:Dmax;
Datahist=hist(Data,DataL);
Datahist=Datahist/sum(Datahist);
% Datahist=cumsum(Datahist);
plot(DataL,Datahist);
xlabel('误差大小/mm');ylabel('概率分布函数');
title(['异常数据直接定位二维误差'])

figure;
Data=err3;
Dmax=min(max(Data),2000);
DataL=0:(Dmax)/50:Dmax;
Datahist=hist(Data,DataL);
Datahist=Datahist/sum(Datahist);
% Datahist=cumsum(Datahist);
plot(DataL,Datahist);
xlabel('误差大小/mm');ylabel('概率分布函数');
title(['异常数据直接定位三维误差']);

%% 异常数据滤除后定位
filedir='异常数据';

err1=[];err2=[];err3=[];
LocEst2=[];
for ii=1:locnum
    for jj=1:size(Obs_1s0(ii).data,1)%数据数目
        Obsz=fitresult( Obs_1s0(ii).data(jj,:));
        LocEst20(jj,:)= UWBLocIters_t(Obsz',para);
        [err10(jj,:),err20(jj),err30(jj)]=errcal(LocEst20(jj,:),locreal(ii,:)*10); 
    end
    err1=[err1;err10];
    err2=[err2 err20];
    err3=[err3 err30];
    LocEst2=[LocEst2;LocEst20];
end

figure;hold on;
xlabel('x坐标/mm');
ylabel('y坐标/mm');
zlabel('z坐标/mm');
axis([0,5000,0,5000,0,3000]);
hold on
plot3(locreal(:,1)*10,locreal(:,2)*10,locreal(:,3)*10,'gp');
plot3(LocEst2(:,1),LocEst2(:,2),LocEst2(:,3),'r.');
plot3(Bsloc(:,1),Bsloc(:,2),Bsloc(:,3),'b^');
grid on 
legend('目标位置','估计位置','锚点位置');
axis([0 5e3 0 5e3 0 3e3]);
title([filedir '异常数据改进定位']);

figure;
for ii=1:3
    subplot(3,1,ii);
    Data=err1(:,ii);
    Dmax=min(max(Data),1000);
    DataL=0:(Dmax)/50:Dmax;
    Datahist=hist(Data,DataL);
    Datahist=Datahist/sum(Datahist);
%     Datahist=cumsum(Datahist);
    plot(DataL,Datahist);
    xlabel('误差大小/mm');ylabel('概率分布函数');
    axis([0 Dmax 0 1])
end
subplot(3,1,1);title([filedir '改进定位x方向误差'])
subplot(3,1,2);title([filedir '改进定位y方向误差'])
subplot(3,1,3);title([filedir '改进定位z方向误差'])

figure;
Data=err2;
Dmax=min(max(Data),1000);
DataL=0:(Dmax)/50:Dmax;
Datahist=hist(Data,DataL);
Datahist=Datahist/sum(Datahist);
% Datahist=cumsum(Datahist);
plot(DataL,Datahist);
xlabel('误差大小/mm');ylabel('概率分布函数');
title([filedir '改进定位二维误差'])

figure;
Data=err3;
Dmax=min(max(Data),2000);
DataL=0:(Dmax)/50:Dmax;
Datahist=hist(Data,DataL);
Datahist=Datahist/sum(Datahist);
% Datahist=cumsum(Datahist);
plot(DataL,Datahist);
xlabel('误差大小/mm');ylabel('概率分布函数');
title([filedir '改进定位三维误差']);