clear;clc;close all;warning off;

Bsnum=4;
Bsloc=[0 0 1200;5000 0 1600;0 3000 1600;5000 3000 1200];

para.Thr = 100;
para.DataLen=1;

para.BsNum=Bsnum;
para.BsLoc=Bsloc;
para.MaxIters=100;
para.xinit = mean(Bsloc,1);
para.dRef=2500;

load fitfunz
Obs_10=[4220  2580 3730 1450;4500 1940 4420 1460;3550 2510 3410 2140;3300 3130 2900 2790;720 4520 3050 5380;
    5100 2220 4970 800;2900 3210 3140 2890;2380 3530 2320 3760;2150 3220 3140 3640;1620 3950 2580 4440];


for ii=1:5
    Obsz=abs(fitresult( Obs_10(ii,:)));
    LocEst1(ii,:)= UWBLocIters(Obsz',para);
    fprintf('无干扰数据%d,估计位置[%01.0f,%01.0f,%01.0f]\n',ii, LocEst1(ii,1), LocEst1(ii,2), LocEst1(ii,3));
end

para2=para;para2.dRef=2.5e3;
for ii=6:10
    Obsz=abs(fitresult( Obs_10(ii,:)));
    LocEst1(ii,:)= UWBLocIters_t(Obsz',para2);
    fprintf('干扰数据%d,估计位置[%01.0f,%01.0f,%01.0f]\n',ii, LocEst1(ii,1), LocEst1(ii,2), LocEst1(ii,3));
end
