clear all;clc;close all;

Bsnum=4;
Bsloc=[0 0 1300;5000 0 1700;0 5000 1700;5000 5000 1300];

filename='动态_final_time.csv';
or_data=csvread(filename,1);%数据预处理
outres=or_data(:,2:5);
t=or_data(:,1)';


% [outres,t]=fileread1('附件5：动态轨迹数据.txt',Bsnum,1,[]);
figure(1);clf;hold on;
plot(t-t(1),outres(:,1));
plot(t-t(1),outres(:,2));
plot(t-t(1),outres(:,3));
plot(t-t(1),outres(:,4));
xlabel('时间');ylabel('测距结果');
legend('A0','A1','A2','A3');

load fitfunz
 
para.Thr = 100;
para.DataLen=1;

para.BsNum=Bsnum;
para.BsLoc=Bsloc;
para.MaxIters=25;
para.xinit = mean(Bsloc,1);
para.dRef=2500;

%% 无视干扰进行定位
cnt=1;
for jj=1:size(outres,1)
    Obsz=fitresult( outres(jj,:));
    LocEst(cnt,:)= UWBLocIters(Obsz',para);
    cnt=cnt+1;
end


%% 进行带干扰分辨的定位
cnt=1;
for jj=1:size(outres,1)
    Obsz=fitresult( outres(jj,:));
    LocEst2(cnt,:)= uwblocestall(Obsz',para);
    cnt=cnt+1;
end
  
%% 进行定位+kalman滤波

%对观测进行等采样间隔插值
T2=t-t(1);
dt=T2(end)/500;
tseq=0:dt:T2(end);
for ii=1:Bsnum
     outres2(:,ii)=interp1(T2, outres(:,ii) ,tseq,'linear');
end
for jj=1:size(outres2,1)
    Obsz2(jj,:)=fitresult( outres2(jj,:));
    [LocEst3(jj,:),type(jj)]= uwblocestall(Obsz2(jj,:),para);
end

figure(2);clf;hold on;
stem(tseq,type==0);
xlabel('时间');ylabel('是否正常');

parak.T = 0.3;  
parak.BsNum=Bsnum;
parak.BsLoc=Bsloc; 
parak.DataLen=size(outres2,1);
parak.Q = eye(6)*25;
parak.Rz=eye(4)*25;
parak.xinit=[LocEst3(1,1:2) 360 0 0 0];
LocEst4 = UWBJZhKalman(Obsz2,type,parak);

 
figure(3);
clf;hold on;
plot3(Bsloc(:,1),Bsloc(:,2),Bsloc(:,3),'b^');
axis([0,5000,0,5000,0,3000]);
plot3(LocEst(:,1),LocEst(:,2),LocEst(:,3),'r.');
plot3(LocEst2(:,1),LocEst2(:,2),LocEst2(:,3),'b.');
plot3(LocEst4(:,1),LocEst4(:,2),LocEst4(:,3),'g.');

p=LocEst;
%二次均匀b样条
re1=[];
for i=1:length(p)-2  
    for t=0:0.01:1   
        
        b0 = 1/2*(1-t)^2;
        b1 = 1/2*(-2*t^2+2*t+1);
        b2 = 1/2*t^2;
        
        x=b0*p(i,1)+b1*p(i+1,1)+b2*p(i+2,1);
        y=b0*p(i,2)+b1*p(i+1,2)+b2*p(i+2,2);        
        z=b0*p(i,3)+b1*p(i+1,3)+b2*p(i+2,3);    
        
        re1=[re1;x y z];
    end
end
% plot3(re1(:,1),re1(:,2),re1(:,3),'r');


p=LocEst2;
%二次均匀b样条
re2=[];
for i=1:length(p)-2  
    for t=0:0.01:1   
        
        b0 = 1/2*(1-t)^2;
        b1 = 1/2*(-2*t^2+2*t+1);
        b2 = 1/2*t^2;
        
        x=b0*p(i,1)+b1*p(i+1,1)+b2*p(i+2,1);
        y=b0*p(i,2)+b1*p(i+1,2)+b2*p(i+2,2);        
        z=b0*p(i,3)+b1*p(i+1,3)+b2*p(i+2,3);    
        
        re2=[re2;x y z];
    end
end

% plot3(re2(:,1),re2(:,2),re2(:,3),'b');

p=LocEst4;
%二次均匀b样条
re3=[];
for i=1:length(p)-2  
    for t=0:0.01:1   
        
        b0 = 1/2*(1-t)^2;
        b1 = 1/2*(-2*t^2+2*t+1);
        b2 = 1/2*t^2;
        
        x=b0*p(i,1)+b1*p(i+1,1)+b2*p(i+2,1);
        y=b0*p(i,2)+b1*p(i+1,2)+b2*p(i+2,2);        
        z=b0*p(i,3)+b1*p(i+1,3)+b2*p(i+2,3);    
        
        re3=[re3;x y z];
    end
end

plot3(re3(:,1),re3(:,2),re3(:,3),'g');
xlabel('x坐标/mm');
ylabel('y坐标/mm');
zlabel('z坐标/mm');
axis([-1000,6000,-1000,6000,-1000,4000]);

% legend('锚点位置','直接定位','去干扰定位','去干扰+卡尔曼滤波','运动轨迹1','运动轨迹2','运动轨迹3');

plot3(Bsloc(:,1),Bsloc(:,2),Bsloc(:,3),'b^');

plot3(Bsloc(:,1),Bsloc(:,2),Bsloc(:,3),'b^');

text(0,0,1300,'A0','FontSize',16)
text(5000,0,1700,'A1','FontSize',16)
text(0,5000,1700,'A2','FontSize',16)
text(5000,5000,1300,'A3','FontSize',16)

% 8个顶点分别为：
% 与(0,0,0)相邻的4个顶点
% 与(a,b,c)相邻的4个顶点
D = [0 0 0;5000 0 0;0 5000 0;0 0 3000;
     5000 5000 3000;0 5000 3000;5000 0 3000;5000 5000 0];
% dot内点分布决定连线顺序
% 连线分了两次，不是必须）
Dot1 = [D(1,1:3);D(4,1:3);D(6,1:3);D(3,1:3);
        D(1,1:3);D(2,1:3);D(7,1:3);D(4,1:3)];
Dot2 = [D(5,1:3);D(8,1:3);D(2,1:3);D(7,1:3);
        D(5,1:3);D(6,1:3);D(3,1:3);D(8,1:3)];
% 一张图上画两次图
hold on;
plot3(Dot1(1:8,1),Dot1(1:8,2),Dot1(1:8,3),'-k');
plot3(Dot2(1:8,1),Dot2(1:8,2),Dot2(1:8,3),'-k');
legend('锚点位置','直接定位','去干扰定位','去干扰+卡尔曼滤波','运动轨迹1','运动轨迹2','运动轨迹3');

grid on 
hold on
