function Xrec = UWBJZhKalman(obs,typeobs,para)

T = para.T;
DataLen = para.DataLen;
xold = para.xinit';
xold2 = xold;
F = [1 0 0 T 0 0;0 1 0 0 T 0;0 0 1 0 0 T;
    0 0 0 1 0 0;0 0 0 0 1 0;0 0 0 0 0 1];

BsNum = para.BsNum;
BsLoc = para.BsLoc;
Q = para.Q;


Xrec = zeros(DataLen-1,3);
Pold = eye(6);
Rz = para.Rz;
valt=[500 350 650];
for ii=1:DataLen-1
    
    Res = UWBToaObs(BsLoc,BsNum,xold(1:3)');
    
    if (typeobs(ii+1)~=0)
        pos=mod(typeobs(ii+1)-1,BsNum)+1;
        val=valt(floor(typeobs(ii+1)/BsNum)+1);
        Res(pos)=Res(pos)+val;
    end
    
    %进行估计
    Err = (Res-obs(ii+1,:));
    Zobs = Err.';
    
    H = Hcal(BsNum,xold(1:3)',BsLoc);
    ErrZ = (Zobs-H*(xold-xold2));
    Pnew = F*Pold*F'+Q;
    Kg = 0.5*Pnew*H'*inv(H*Pnew*H'+Rz);
    %进行状态更新
    xnew1 = F*xold;
    xnew = xnew1-Kg*ErrZ;%进行补偿
    
    Pold = (eye(6)-Kg*H)*Pnew;
    xold2 = xold;
    xold = xnew;
    Xrec(ii,:) = xold(1:3)';
    xdebug(ii,:) = xnew';
end

aaa=1;
end




function H = Hcal(bsNum,loc,BsLoc)
H = zeros(bsNum,6);
dd = 0.5;
Res0 = UWBToaObs(BsLoc,bsNum,loc);
Resx1 = UWBToaObs(BsLoc,bsNum,loc+[dd 0 0]);
Resy1 = UWBToaObs(BsLoc,bsNum,loc+[0 dd 0]);
Resz1 = UWBToaObs(BsLoc,bsNum,loc+[0 0 dd]);
H(:,1)=(Resx1-Res0)'/dd;
H(:,2)=(Resy1-Res0)'/dd;
H(:,3)=(Resz1-Res0)'/dd;

end