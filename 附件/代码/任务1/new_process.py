# 导入工具包
import pandas as pd
import numpy as np
import os
import csv
# 路径
# path = 'D:/file/数学建模_2021/2021年中国研究生数学建模竞赛赛题/2021年E题/附件1：UWB数据集/异常数据/'
path = 'D:/file/数学建模_2021/2021年中国研究生数学建模竞赛赛题/2021年E题/'
#设置行不限制数量
pd.set_option('display.max_rows',None)
#最后的的参数可以限制输出行的数量
#设置列不限制数量
pd.set_option('display.max_columns',None)
#最后的的参数可以限制输出列的数量`

# 文件列表
files = []
for file in os.listdir(path):
    if file.endswith(".txt"):
        files.append(path + file)
# 定义一个空的dataframe
# 遍历所有文件
for file in files:
    print(file)
    csv_name=file[file.rfind('/', 1) + 1:file.rfind('.', 1)]+".csv"
    f = open(csv_name, 'w', newline='', encoding='utf-8')
    csv_writer = csv.writer(f)
    csv_writer.writerow(["T","A0", "A1", "A2", "A3"])
    data = pd.read_table(file, header=None)
    num=len(data)-1
    # print(num/4)
    print(data)
    j=1
    for i in range(1,int(num/4)):
        j = (i - 1) * 4 + 1
        if data[0][j].split(':')[1]==data[0][j+1].split(':')[1]==data[0][j+2].split(':')[1]==data[0][j+3].split(':')[1]:
            A = [data[0][j].split(':')[1], data[0][j].split(':')[5], data[0][j + 1].split(':')[5], data[0][j + 2].split(':')[5],
                 data[0][j + 3].split(':')[5]]
            csv_writer.writerow(A)



