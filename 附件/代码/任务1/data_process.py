import csv
import pandas as pd

# #设置行不限制数量
# pd.set_option('display.max_rows',None)
# #最后的的参数可以限制输出行的数量
# #设置列不限制数量
# pd.set_option('display.max_columns',None)
# #最后的的参数可以限制输出列的数量`


data = pd.read_csv("附件5：动态轨迹数据.csv")
print(data)

# data1 = data.loc[0:len(data), ['A0', 'A1', 'A2', 'A3']]
# print(data1)
# print(data.duplicated(subset=['A0', 'A1', 'A2', 'A3']))

data = data.drop_duplicates(subset=['A0', 'A1', 'A2', 'A3'])
print(data)

data_A0 = data.loc[:, ['A0']]
print(data_A0)
print(data_A0.describe())
data_A0_z_col = "A0_z"
data_A0_d_col = "A0"
data_A0_pl = 3
data_A0[data_A0_z_col] = (data_A0[data_A0_d_col] - data_A0[data_A0_d_col].mean()) / data_A0[data_A0_d_col].std()
A0_OUT = data_A0.loc[(data_A0[data_A0_z_col] > data_A0_pl) | (data_A0[data_A0_z_col] < -data_A0_pl)]
print(A0_OUT)
data = data.drop(A0_OUT.index)


data_A1 = data.loc[:, ['A1']]
print(data_A1)
print(data_A1.describe())
data_A1_z_col = "A1_z"
data_A1_d_col = "A1"
data_A1_pl = 3
data_A1[data_A1_z_col] = (data_A1[data_A1_d_col] - data_A1[data_A1_d_col].mean()) / data_A1[data_A1_d_col].std()
A1_OUT = data_A1.loc[(data_A1[data_A1_z_col] > data_A1_pl) | (data_A1[data_A1_z_col] < -data_A1_pl)]
print(A1_OUT)
data = data.drop(A1_OUT.index)


data_A2 = data.loc[:, ['A2']]
print(data_A2)
print(data_A2.describe())
data_A2_z_col = "A2_z"
data_A2_d_col = "A2"
data_A2_pl = 3
data_A2[data_A2_z_col] = (data_A2[data_A2_d_col] - data_A2[data_A2_d_col].mean()) / data_A2[data_A2_d_col].std()
A2_OUT = data_A2.loc[(data_A2[data_A2_z_col] > data_A2_pl) | (data_A2[data_A2_z_col] < -data_A2_pl)]
print(A2_OUT)
data = data.drop(A2_OUT.index)


data_A3 = data.loc[:, ['A3']]
print(data_A3)
print(data_A3.describe())
data_A3_z_col = "A3_z"
data_A3_d_col = "A3"
data_A3_pl = 3
data_A3[data_A3_z_col] = (data_A3[data_A3_d_col] - data_A3[data_A3_d_col].mean()) / data_A3[data_A3_d_col].std()
A3_OUT = data_A3.loc[(data_A3[data_A3_z_col] > data_A3_pl) | (data_A3[data_A3_z_col] < -data_A3_pl)]
print(A3_OUT)
# print(A3_OUT.index)
data = data.drop(A3_OUT.index)


print(data)


data.to_csv('动态_final_time.csv', index=None, encoding="utf-8")
data = data.loc[:, ['A0', 'A1', 'A2', 'A3']]
data.to_csv('动态_final_notime.csv', index=None, encoding="utf-8")
